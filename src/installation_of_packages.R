##setting the working directory
## If you don't have the packages being used use the script to load them.

if (!requireNamespace("BiocManager", quietly = TRUE))
  install.packages("BiocManager")
##installing limma package
BiocManager::install("limma")
BiocManager::install("edgeR")
BiocManager::install("DESeq2")
BiocManager::install("Biobase")
BiocManager::install("Biobase")


