
getwd()

#load libraries
library(edgeR)
library(DESeq2)
library(limma)
library(Biobase)
library(gplots)

smear_plots <- function( fit_model, p_value, name){
  deGenes <- decideTestsDGE(fit_model, p=p_value)
  deGenes <- rownames(fit_model)[as.logical(deGenes)]
  plotSmear(lrt.edgeR, de.tags=deGenes, main = name)
  abline(h=c(-1, 1), col=2)
  
}

diff_deseq2 <- function(data, meta) {
  # function to print x raised to the power y
  dds <- DESeqDataSetFromMatrix(countData = data, colData = meta, 
                                design = ~ strain )
  dds <- DESeq(dds)
  
  # Plot dispersion estimates
  plotDispEsts(dds)
  
  ## DESeq2 ##
  contrast.deseq2 <- list("strain_DBA.2J_vs_C57BL.6J")
  deseq2_results <- results(dds, contrast=contrast.deseq2)
  deseq2_results$threshold <- as.logical(deseq2_results$padj < p.threshold)
  
  return (row.names(deseq2_results)[which(deseq2_results$threshold)])
  
}

diff_edgeR <- function(data, meta) {
  ## DE with edgeR
  dge <- DGEList(counts=data, group=meta$strain)
  
  # Normalize by total count
  dge <- calcNormFactors(dge)     #uses TMM method.
  
  # Create the contrast matrix
  design.mat <- model.matrix(~ 0 + dge$samples$group)
  colnames(design.mat) <- levels(dge$samples$group)
  
  # Estimate dispersion parameter for GLM
  dge <- estimateGLMCommonDisp(dge, design.mat)
  dge <- estimateGLMTrendedDisp(dge, design.mat, method="power")
  dge<- estimateGLMTagwiseDisp(dge,design.mat)
  
  # Plot mean-variance
  plotBCV(dge)
  
  # Design matrix
  design.mat <- model.matrix(~ 0 + dge$samples$group)
  colnames(design.mat) <- c("C57BL", "DBA")
  # Model fitting
  fit.edgeR <- glmFit(dge, design.mat)
  # Differential expression
  contrasts.edgeR <- makeContrasts(C57BL - DBA, levels=design.mat)
  lrt.edgeR <- glmLRT(fit.edgeR, contrast=contrasts.edgeR)
  
  # Access results tables
  edgeR_results <- lrt.edgeR$table
  sig.edgeR <- decideTestsDGE(lrt.edgeR, adjust.method="BH", p.value = p.threshold)
  
  return (row.names(edgeR_results)[which(sig.edgeR != 0)])
  
}

diff_limmaV <- function(data, meta) {
  # Create design matrix
  design <- model.matrix(~ meta$strain)
  
  # Apply voom transformation
  nf <- calcNormFactors(data)
  par(mfrow=c(1,2))
  v <- voom(data, design, lib.size=colSums(data)*nf, 
            normalize.method="quantile", plot=TRUE)
  
  # Create design matrix
  design <- model.matrix(~ pData(bottomly.eset)$strain)
  # Usual limma pipeline
  fit.voom <- lmFit(v, design)
  fit.voom <- eBayes(fit.voom)
  plotSA(fit.voom, main="Final model: Mean-variance trend")
  
  voom_results <- topTable(fit.voom, coef=2,  adjust="BH", number = nrow(exprs(bottomly.eset)))
  
  voom_results$threshold <- as.logical(voom_results$adj.P.Val < p.threshold)
  
  return (row.names(voom_results)[which(voom_results$threshold)])
  
}

p.threshold <- 0.05

## timming deseq2
start_time <- Sys.time()
genes.deseq <- diff_deseq2(data, meta)
end_time <- Sys.time()
end_time - start_time

## timming edgeR
start_time <- Sys.time()
genes.edgeR <- diff_edgeR(data, meta)
end_time <- Sys.time()
end_time - start_time

##timing limma_voom differential expression
start_time <- Sys.time()
genes.voom <- diff_limmaV(data, meta)
end_time <- Sys.time()
end_time - start_time

##plotting the smear plots for the algorithms: 

par(mfrow=c(1,3))
smear_plots(lrt.edgeR, p.threshold, "edgeR")       #edgeR
plotMA(deseq2_results, main = "DESeq2 log2fold change") #DESeq2
limma::plotMA(fit.voom, main = "Limma package") #DESeq2

## the length of significant teaching.
length(genes.edgeR)
length(genes.deseq)
length(genes.voom)

## the venn diagram for overlapping genes.
venn(list(edgeR = genes.edgeR, DESeq2 = genes.deseq, voom = genes.voom))