The project is for doing differential gene analysis using three methods limma, edgeR and DESeq2. 

To run the project clone the repository into your working directory, using the git clone https://gitlab.msu.edu/lesiyonr/cmse410_project.git. 

After that run the package installation script for installing the required packages. The script is installation_of_packages.R. 

        It installs the packages: Bio
            ```
            if (!requireNamespace("BiocManager", quietly = TRUE))
                install.packages("BiocManager")
            ##installing limma package
            BiocManager::install("limma")
            BiocManager::install("edgeR")
            BiocManager::install("DESeq2")
            BiocManager::install("Biobase")
            BiocManager::install("Biobase")

            ```
        If this packages are already installed in your R session, then you don't have to run the command. 

Next run the script data_processing.R to load the data into your working directory.. 

And then run the differential_gene_expression.R analysis script to do differential gene analysis for all the three methods explore for this project.
